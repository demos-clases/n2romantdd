﻿using System;
using nromanos;

namespace nromanos_test
{
	public class ToRomanTest
	{
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Integer2RomanLessThan10Spec()
        {
            ToRoman toRoman = new ToRoman();
            string roman1 = toRoman.FromInt(1);
            string roman2 = toRoman.FromInt(2);
            string roman3 = toRoman.FromInt(3);
            string roman4 = toRoman.FromInt(4);
            string roman5 = toRoman.FromInt(5);
            string roman6 = toRoman.FromInt(6);
            string roman7 = toRoman.FromInt(7);
            string roman8 = toRoman.FromInt(8);
            string roman9 = toRoman.FromInt(9);
            string roman10 = toRoman.FromInt(10);

            Assert.AreEqual("I", roman1);
            Assert.AreEqual("II", roman2);
            Assert.AreEqual("III", roman3);
            Assert.AreEqual("IV", roman4);
            Assert.AreEqual("V", roman5);
            Assert.AreEqual("VI", roman6);
            Assert.AreEqual("VII", roman7);
            Assert.AreEqual("VIII", roman8);
            Assert.AreEqual("IX", roman9);
            Assert.AreEqual("X", roman10);
        }

        [Test]
        public void Integer2RomanGreaterThan10Spec()
        {
            ToRoman toRoman = new ToRoman();
            string roman11 = toRoman.FromInt(11);
            string roman12 = toRoman.FromInt(12);
            string roman13 = toRoman.FromInt(13);
            string roman14 = toRoman.FromInt(14);
            string roman15 = toRoman.FromInt(15);
            string roman16 = toRoman.FromInt(16);
            string roman17 = toRoman.FromInt(17);
            string roman18 = toRoman.FromInt(18);
            string roman19 = toRoman.FromInt(19);
            string roman20 = toRoman.FromInt(20);

            Assert.AreEqual("XI", roman11);
            Assert.AreEqual("XII", roman12);
            Assert.AreEqual("XIII", roman13);
            Assert.AreEqual("XIV", roman14);
            Assert.AreEqual("XV", roman15);
            Assert.AreEqual("XVI", roman16);
            Assert.AreEqual("XVII", roman17);
            Assert.AreEqual("XVIII", roman18);
            Assert.AreEqual("XIX", roman19);
            Assert.AreEqual("XX", roman20);
        }

        [Test]
        public void Integer2RomanGreaterLess100Spec()
        {
            ToRoman toRoman = new ToRoman();
            string roman20 = toRoman.FromInt(20);
            string roman30 = toRoman.FromInt(30);
            string roman40 = toRoman.FromInt(40);
            string roman50 = toRoman.FromInt(50);
            string roman23 = toRoman.FromInt(23);
            string roman37 = toRoman.FromInt(37);
            string roman48 = toRoman.FromInt(48);
            string roman51 = toRoman.FromInt(51);
            string roman68 = toRoman.FromInt(68);
            string roman99 = toRoman.FromInt(99);

            Assert.AreEqual("XX", roman20);
            Assert.AreEqual("XXX", roman30);
            Assert.AreEqual("XL", roman40);
            Assert.AreEqual("L", roman50);
            Assert.AreEqual("XXIII", roman23);
            Assert.AreEqual("XXXVII", roman37);
            Assert.AreEqual("XLVIII", roman48);
            Assert.AreEqual("LI", roman51);
            Assert.AreEqual("LXVIII", roman68);
            Assert.AreEqual("XCIX", roman99);
        }
    }
}
