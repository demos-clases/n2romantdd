﻿using System;
namespace nromanos
{
	public class ToRoman
	{
		public string FromInt(int n)
        {
            if (n == 1) return "I";
            else if (n == 2) return "II";
            else if (n == 3) return "III";
            else if (n == 4) return "IV";
            else if (n == 5) return "V";
            else if (n > 5 && n < 9) return FromInt(5) + FromInt(n - 5);
            else if (n == 9) return "IX";
            else if (n == 10) return "X";
            else if (n > 10 && n < 20) return FromInt(10) + FromInt(n - 10);
            else if (n == 20) return "XX";
            else if (n == 30) return "XXX";
            else if (n == 40) return "XL";
            else if (n == 50) return "L";
            else if (n == 60) return "LX";
            else if (n == 70) return "LXX";
            else if (n == 80) return "LXXX";
            else if (n == 90) return "XC";
            else if (n == 100) return "C";
            else if (n > 10 && n < 100)
            {
                int decenas = n - (n%10);
                int unidades = n % 10;
                return FromInt(decenas) + FromInt(unidades);
            }
            else return "-";
        }
	}
}

